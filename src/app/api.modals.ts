

export interface Specialization {
    code: string,
    name: string,
    note: string;
}

export interface Doctor {
    name: string;
    emailId: string;
    address: string;
    mobile: string;
    gender: string;
    note: string;
    specializationId: number;
}

export interface Patient {
    patientName: string,
    patientEmail: string,
    patientAddress: string,
    patientMobile: string,
    patientPassword: string
}

export interface Appointment {
    noOfSlots?: number,
    details: string,
    fee?: number,
    doctorId?: number,
    date?: Date


}

export interface ResponseHandler {

    timestamp: string;
    status: string;
    isSucess: string;
    message: string;
    data: {
        id: Number
        code: string
        name: string
        note: string
    };
}
