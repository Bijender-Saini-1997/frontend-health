import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppointmentDetailsComponent } from './components/appointment-page/appointment-details/appointment-details.component';
import { AppointmentRegisterComponent } from './components/appointment-page/appointment-register/appointment-register.component';
import { DoctorRegisterComponent } from './components/doctor-page/doctor-register/doctor-register.component';
import { PatientDetailsComponent } from './components/patient-page/patient-details/patient-details.component';
import { PatientRegisterComponent } from './components/patient-page/patient-register/patient-register.component';
import { SpecializationRegisterComponent } from './components/specialization-page/specialization-register/specialization-register.component';

const routes: Routes = [
  {path:"specialization/register",component:SpecializationRegisterComponent},
  {path:"doctor/register",component:DoctorRegisterComponent},
  {path:"patient/register",component:PatientRegisterComponent},
  {path:"patient/details",component:PatientDetailsComponent},
  {path:"appointment/register",component:AppointmentRegisterComponent},
  {path:"appointment/details",component:AppointmentDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
