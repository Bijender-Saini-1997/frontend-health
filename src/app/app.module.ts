import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DoctorRegisterComponent } from './components/doctor-page/doctor-register/doctor-register.component';
import { SpecializationRegisterComponent } from './components/specialization-page/specialization-register/specialization-register.component';
import { PatientRegisterComponent } from './components/patient-page/patient-register/patient-register.component';
import { PatientDetailsComponent } from './components/patient-page/patient-details/patient-details.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { AppointmentRegisterComponent } from './components/appointment-page/appointment-register/appointment-register.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import { AppointmentDetailsComponent } from './components/appointment-page/appointment-details/appointment-details.component';
@NgModule({
  declarations: [
    AppComponent,
    DoctorRegisterComponent,
    SpecializationRegisterComponent,
    PatientRegisterComponent,
    PatientDetailsComponent,
    AppointmentRegisterComponent,
    AppointmentDetailsComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
   MatSelectModule,
   MatFormFieldModule,
   MatRadioModule,
   MatCardModule,
   MatButtonModule,
   HttpClientModule,
   MatTableModule,
   MatIconModule,
   MatSnackBarModule,
   MatToolbarModule,
   MatDatepickerModule,
   MatNativeDateModule
   
  

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
