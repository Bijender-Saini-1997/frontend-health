import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Appointment, Doctor, Patient, ResponseHandler, Specialization } from "./api.modals";

@Injectable({
    providedIn: 'root'
})

export class ApiService {

    baseUrl: string = 'http://localhost:9090'
    constructor(private httpClient: HttpClient) {

    }

    public registerSpecialization(specialization: Specialization): Observable<ResponseHandler> {
        return this.httpClient.post<ResponseHandler>(`${this.baseUrl}/specialization/register`, specialization);
    }
    
    
    public getSpecializationIdAndName(){
        return this.httpClient.get<ResponseHandler>(`${this.baseUrl}/getSpecializationIdAndName`);
        
    }
    
    
    public registerDoctor(doctorObj:Doctor){
        return this.httpClient.post<ResponseHandler>(`${this.baseUrl}/saveDoctor`,doctorObj);
        
    }
    
    /*
    * patient rest node
    */
   
   registerPatient(patient:Patient){
       console.log(patient);
       return this.httpClient.post<ResponseHandler>(`${this.baseUrl}/patient/register`,patient);
    }
    
    
    getAllPatient(){
       return this.httpClient.get<ResponseHandler>(`${this.baseUrl}/patient/getAllPatient`);
       
    }
    
    getPatient(id:number){
       return this.httpClient.get<ResponseHandler>(`${this.baseUrl}/patient/getPatient/${id}`);
       
    }
    
    deletePatient(id:number){
       return this.httpClient.delete<ResponseHandler>(`${this.baseUrl}/patient/deletePatient/${id}`);
       
    }
    
    updatePatient(id:number,patient:Patient){
       return this.httpClient.put<ResponseHandler>(`${this.baseUrl}/patient/updatePatient/${id}`,patient);

   }


//    Appointment 

getDoctorIdAndName(){
    return this.httpClient.get<ResponseHandler>(`${this.baseUrl}/appointment/getDoctorIdAndName`);
    
}

  registerAppointment(appointment:Appointment){
    return this.httpClient.post<ResponseHandler>(`${this.baseUrl}/appointment/register`,appointment);

  }
  fetchAllAppointment(){
    return this.httpClient.get<ResponseHandler>(`${this.baseUrl}/appointment/getAllAppointment`);

  }

}