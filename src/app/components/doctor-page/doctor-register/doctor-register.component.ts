import { Component, OnInit } from '@angular/core';
import { Doctor } from 'src/app/api.modals';
import { ApiService } from 'src/app/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-doctor-register',
  templateUrl: './doctor-register.component.html',
  styleUrls: ['./doctor-register.component.scss']
})
export class DoctorRegisterComponent implements OnInit {

  constructor(private apiService: ApiService) { }
  specializationDropDownData: any;
  doctorObj: Doctor = { name: '', emailId: '', address: '', mobile: '', gender: '', specializationId: 0, note: '' }
  ngOnInit(): void {
    this.getSpecializationIdAndName();
  }

  registerDoctor() {
    this.apiService.registerDoctor(this.doctorObj).subscribe(data => {
      Swal.fire('success', data.message, 'success');
      console.log(data);
    })
  }

  getSpecializationIdAndName() {
    this.apiService.getSpecializationIdAndName().subscribe(data => {
      this.specializationDropDownData = data.data;
      console.log(this.specializationDropDownData);
    }

    );
  }

}
