import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { PatientTableColumn } from 'src/app/app.enum';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.scss']
})
export class PatientDetailsComponent implements OnInit {
  patientList: any;
  patientTableColumn = [PatientTableColumn.PATIENT_ID, PatientTableColumn.NAME, PatientTableColumn.EMAIL,
  PatientTableColumn.MOBILE, PatientTableColumn.ADDRESS, PatientTableColumn.ACTION];
  message: any
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.fetchAllPatient();
  }

  fetchAllPatient() {
    this.apiService.getAllPatient().subscribe(data => {
      this.patientList = data.data;
    })
  }

  deletePatient(id: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You want to delete it",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.apiService.deletePatient(id).subscribe(data => {
          this.message = data.message,
            this.fetchAllPatient();
        });
        Swal.fire(
          'Deleted!',
          this.message
          ,
          'success'
        )

      }

    })
  }

}
