import { Component, OnInit } from '@angular/core';
import { Patient } from 'src/app/api.modals';
import { ApiService } from 'src/app/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-patient-register',
  templateUrl: './patient-register.component.html',
  styleUrls: ['./patient-register.component.scss']
})
export class PatientRegisterComponent implements OnInit {

  patientObj: Patient = { patientName: '', patientEmail: '', patientAddress: '', patientMobile: '', patientPassword: '' }
  constructor(private apiService:ApiService) { }


  registerPatient() {
  this.apiService.registerPatient(this.patientObj).subscribe(data=>{
    Swal.fire('success', data.message, 'success');
    console.log(data);
  })
  }

  ngOnInit(): void {
  }

}
