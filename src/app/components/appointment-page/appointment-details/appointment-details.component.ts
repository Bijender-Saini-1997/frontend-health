import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { AppointmentTableColumn } from 'src/app/app.enum';

@Component({
  selector: 'app-appointment-details',
  templateUrl: './appointment-details.component.html',
  styleUrls: ['./appointment-details.component.scss']
})
export class AppointmentDetailsComponent implements OnInit {
  appointmentList: any;
  constructor(private apiService: ApiService) { }

  appointmentcolumn= [AppointmentTableColumn.ID, AppointmentTableColumn.SLOTS,AppointmentTableColumn.DETAILS,
  AppointmentTableColumn.AMOUNT, AppointmentTableColumn.DATE];

  fetchAllAppointment() {
    this.apiService.fetchAllAppointment().subscribe(data => {
      this.appointmentList=data.data;
      console.log(data);
    })

  }
  ngOnInit(): void {
    this.fetchAllAppointment();
  }

}
