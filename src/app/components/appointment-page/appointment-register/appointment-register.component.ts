import { Component, OnInit } from '@angular/core';
import { Appointment } from 'src/app/api.modals';
import { ApiService } from 'src/app/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-appointment-register',
  templateUrl: './appointment-register.component.html',
  styleUrls: ['./appointment-register.component.scss']
})
export class AppointmentRegisterComponent implements OnInit {
  doctorDropDownData: any;
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getDoctorIdAndName();
  }

  appointment: Appointment = { details: '' };

  getDoctorIdAndName() {
    this.apiService.getDoctorIdAndName().subscribe(data => {
      this.doctorDropDownData = data.data;
      console.log(data);
    })
  }

  registerAppointment() {
    this.apiService.registerAppointment(this.appointment).subscribe(data=>{
      Swal.fire('success', data.message, 'success');
    });
 
  }

}
