import { Component, OnInit } from '@angular/core';
import { Specialization } from 'src/app/api.modals';
import { ApiService } from 'src/app/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-specialization-register',
  templateUrl: './specialization-register.component.html',
  styleUrls: ['./specialization-register.component.scss']
})
export class SpecializationRegisterComponent implements OnInit {

  specializationObj: Specialization = { code: '', name: '', note: '' };
  constructor( private apiService:ApiService) { }

  ngOnInit(): void {
  }

  registerSpecialization(){
    this.apiService.registerSpecialization(this.specializationObj).subscribe(data=>{
      Swal.fire('success', data.message, 'success');
      console.log(data);
    })
  }

}
