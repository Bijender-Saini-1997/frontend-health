import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecializationRegisterComponent } from './specialization-register.component';

describe('SpecializationRegisterComponent', () => {
  let component: SpecializationRegisterComponent;
  let fixture: ComponentFixture<SpecializationRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpecializationRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecializationRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
