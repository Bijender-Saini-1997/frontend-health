

export enum PatientTableColumn {
    PATIENT_ID = 'Patient Id',
    NAME = 'Name',
    EMAIL = 'Email',
    MOBILE = 'Mobile',
    ADDRESS = 'Address',
    ACTION = 'Action'
}

export enum AppointmentTableColumn {

    ID = 'Appointmnet Id',
    NAME = 'Doctor Name',
    SLOTS ='Appointment Slots',
    AMOUNT = 'Amount',
    DETAILS = 'Details',
    DATE = 'Date',


}